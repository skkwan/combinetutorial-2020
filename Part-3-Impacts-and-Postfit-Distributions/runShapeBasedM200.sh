# runShapeBasedM200.sh 

cmsenv


# Convert the text datacard into a RooFit workspace ourselves instead of combine doing it internally every time we run
text2workspace.py datacard_part3.txt -m 200 -o workspace_part3.root

# Perform an initial fit for the signal strength and its uncertainty
combineTool.py -M Impacts -d workspace_part3.root -m 200 --rMin -1 --rMax 2 --robustFit 1 --doInitialFit

# Run impacts for all the nuisance parameters
combineTool.py -M Impacts -d workspace_part3.root -m 200 --rMin -1 --rMax 2 --robustFit 1 --doFits

# This will take a little bit of time. When finished we collect all the output and convert it to a json file:
combineTool.py -M Impacts -d workspace_part3.root -m 200 --rMin -1 --rMax 2 --robustFit 1 --output impacts.json

# We can then make a plot showing the pulls and parameter impacts, sorted by the largest impact:
plotImpacts.py -i impacts.json -o impacts

# Produce pre-fit and post-fit distributions
combine -M FitDiagnostics workspace_part3.root -m 200 --rMin -1 --rMax 2 --saveShapes --saveWithUncertainties
