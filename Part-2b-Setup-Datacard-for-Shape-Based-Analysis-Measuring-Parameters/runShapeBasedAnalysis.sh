# runShapeBasedAnalysis.sh

cmsenv

# Convert the text datacard into a RooFit workspace ourselves instead of combine doing it internally every time we run
text2workspace.py datacard_shape_part2.txt -m 800 -o workspace_part2.root

# Use this as input to combine instead of the text datacard
# n.b. set POI r range to [20, 20]
combine -M FitDiagnostics --rMin -20 --rMax 20 workspace_part2.root -m 800

# Compare the pre- and post-fit values of the parameters, to see how much the fit to data has shifted
# and constrained these parameters with respect to the input uncertainty
python ../diffNuisances.py fitDiagnostics.root --all
