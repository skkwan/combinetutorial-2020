#-------------------------------------------------#
# part-1e-roofit-fit-manual-likelihood-multiple-nuisances.py
# https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/tutorial2020/exercise/#e-fitting
#-------------------------------------------------#

from ROOT import *

#--------------------------#
# (E) Fitting
#--------------------------#  

ws = RooWorkspace("ws")

# 1. Expression for the number of events in our model
ws.factory('expr::nEvents("mu * s * pow(1.025, lumi) * pow(1.05, sigth) + \
                           ztt * pow(1.025, lumi) * pow(1.04, zttxs) + \
                           tt  * pow(1.025, lumi) * pow(1.06, ttxs)",  \
                           mu[1.0, 0.0, 4.0],  \
                           s[5], \
                           ztt[3.7], \
                           tt[4.4], \
                           sigth[0, -4, 4],  \
                           zttxs[0, -4, 4],  \
                           ttxs[0, -4, 4],   \
                           lumi[0, -4, 4])')


# 2. And add a unit Gaussian constraint
ws.factory('Gaussian::lumiconstr(lumi, 0, 1)')
ws.factory('Gaussian::sigthconstr(sigth, 0, 1)')
ws.factory('Gaussian::zttxsconstr(zttxs, 0, 1)')
ws.factory('Gaussian::ttxsconstr(ttxs, 0, 1)')

# 3. Our full likelihood will now be:
# - PROD::name(pdf1,pdf2] : Create product of p.d.f with 'name' with given input p.d.fs
ws.factory('Poisson::poisN(N[15], nEvents)')
ws.factory('PROD::likelihood(poisN, lumiconstr, sigthconstr, zttxsconstr, ttxsconstr)')

# 4. And the NLL is:
ws.factory('expr::NLL("-log(@0)", likelihood)')

# 5. Minimize:
nll = ws.function("NLL")   # retrieve the function with the given name                
minim = RooMinimizer(nll)  # RooMinimizer minimizes any RooAbsReal func. w.r.t its parameters    
minim.setErrorLevel(0.5)   # Wilks' theorem: -Delta NLL < 0.5                                    
minim.minimize("Minuit2", "migrad")    # this minimizes the function in the constructor             
bestfitnll = nll.getVal()  # getVal() directly from the function  

# 6. Make a graph out of a scan of Delta NLL (= log of profile likelihood ratio), 
#    by performing fits for different values of mu, where mu is fixed.

gr = TGraph()
npoints = 0
for i in range(0, 60):
    npoints += 1
    mu = 0.05*i
    # perform fits for different values of mu with mu fixed
    ws.var("mu").setConstant(True)
    ws.var("mu").setVal(mu)

    deltanll = nll.getVal() - bestfitnll
    print(deltanll)
    
    gr.SetPoint(npoints, mu, deltanll)

canv = TCanvas();
gr.GetXaxis().SetTitle("Signal strength #mu")
gr.GetYaxis().SetTitle("#Delta NLL")
gr.Draw("ALP");
canv.SaveAs("likelihoodscan.pdf")


