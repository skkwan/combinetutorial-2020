#---------------------------------------------------------------------------#
# part-1-roofit.py
# https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/tutorial2020/exercise/#part-1-roofit
#---------------------------------------------------------------------------# 

from ROOT import *
gROOT.SetBatch(True)

#-----------------#
# (A) Variables
#-----------------# 

# Mass of the Hypothetical boson
MH = RooRealVar("MH", "mass of the Hypothetical Boson (H-boson) in GeV",
                125, 120, 130)

# Invariant mass
mass = RooRealVar("mass", "invariant mass of decay products of the H-boson in GeV",
                  100, 80, 200)

# Resolution of the measurement of the invariant mass
sigma = RooRealVar("sigma", "resolution of the measurement of the invariant mass",
                   10, 0, 20)


#-------------------------# 
# (B) Functions and PDFs
#-------------------------# 

# - Make a function out of the variables which represent the relative resolution as a
#   function of the hypothetical mass MH. Note "sigma" and "mass" defined above.

func = RooFormulaVar("relativeResolution", "@0/@1", RooArgList(sigma, mass))
# func.Print("v")

# - The main objects we want from RooFit are probability density functions. 
#   Let's make a Gaussian PDF for the PDF of observing an invariant mass,
#   given the true H-boson mass and a relative resolution sigma

# - Syntax: RooGaussian (const char *name, const char *title,
#                        RooAbsReal &_x, RooAbsReal &_mean, RooAbsReal &_sigma)
gauss = RooGaussian("gauss", "f(m|M_{H}, #sigma)", mass, MH, sigma)


#-------------------------#
# (C) Plotting
#-------------------------#

# - We need a canvas and a RooPlot object, which needs to be a method associated
#   with the observable

can = TCanvas()
massPlot = mass.frame()

# - Now we can plot the Gaussian PDF for several mass values

MH.setVal(130)
gauss.plotOn(massPlot, RooFit.LineColor(kGreen+3))

MH.setVal(120)
gauss.plotOn(massPlot, RooFit.LineColor(kBlue))

MH.setVal(125)
gauss.plotOn(massPlot, RooFit.LineColor(kRed))

MH.setVal(115)
gauss.plotOn(massPlot, RooFit.LineColor(kGreen), RooFit.LineStyle(2))  # dashed green line

massPlot.Draw()
can.Draw()
can.SaveAs("massGaussians.pdf")

#-------------------------# 
# (D) Workspaces
#-------------------------# 

# RooWorkspaces allow us to store RooFit objects and links between them.
# Let's create one:

ws = RooWorkspace("w")
getattr(ws, 'import')(gauss)   # the associated RooRealVars will also be imported
getattr(ws, 'import')(func)

ws.writeToFile("workspace.root")

