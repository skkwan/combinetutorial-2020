#-------------------------------------------------#
# part-1e-roofit-fitting.py
# https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/tutorial2020/exercise/#e-fitting
#-------------------------------------------------#

from ROOT import *

#--------------------------#
# (E) Fitting
#--------------------------#  

# The ultimate goal of Combine is to do fits with variables and pdfs. Models
# that we define in Combine make use of likelihood fitting, so let's build a 
# likelihood: 

# POI: mu, nuisance: theta, constraint terms pi(theta0 | theta)

# Let's build the likelihood by hand for a 1-bin counting experiment, with one
# signal process and one background process. We'll assume no nuisance parameters
# for now.

# 1. Number of observed events in data: 15
# - Expected number of signal events: 5
# - Expected number of background events: 8.1

# 2. It's easiest to use the RooFit workspace factory to build our model
ws = RooWorkspace("ws")

# # 3. Create an expression for the number of events in our model: mu*s + b
# #   - Syntax: expr::name(<expr>,var,...] creates a generic function that interprets the
# #             given expression.
# #   - Syntax: x[3,-10,10] creates variable x with range (-10, 10) and initial value 3 and
# #             puts it in the workspace.
# #   - Syntax: x[3] creates variable x with given constant value.
# ws.factory('expr::nEvents("mu*s + b", mu[1.0, 0, 4], s[5], b[8.1])')

# # 4. Now we can build the likelihood which is just our poisson pdf:
# #   - Syntax: poisN is our name for this distribution, presumably N[15] is a fixed 
# #     number (# of observed events), and "nEvents" is the (mu*S + B) we defined earlier.
# ws.factory('Poisson::poisN(N[15], nEvents)')

# # 5. To find the best-fit value for our parameter of interest mu, we need to maximize
# #    the likelihood. In practice we minimize the NLL (negative log of the likelihood).
# #    So define the expression.
# ws.factory('expr::NLL("-log(@0)", poisN)')

# # 6. We can now use the RooMinimizer to find the minimum of the NLL.

# nll = ws.function("NLL")   # retrieve the function with the given name
# minim = RooMinimizer(nll)  # RooMinimizer minimizes any RooAbsReal func. w.r.t its parameters
# minim.setErrorLevel(0.5)   # Wilks' theorem: -Delta NLL < 0.5
# minim.minimize("Minuit2", "migrad")    # this minimizes the function in the constructor
#                                        # migrad is used to extract the confidence interval
# bestfitnll = nll.getVal()  # getVal() directly from the function

# 7. Now let's add a nuisance parameter, lumi, representing the luminosity uncertainty.
#    The uncertainty will be log-normally distributed, with a 2.5% effect on both the signal
#    and the background.

# - We should modify our expression for the number of events in the model
ws.factory('expr::nEvents("mu*s*pow(1.025, lumi) + b*pow(1.025, lumi)", mu[1.0, 0.0, 4.0], s[5], b[8.1], lumi[0, -4, 4])')

# 8. And add a unit Gaussian constraint
ws.factory('Gaussian::lumiconstr(lumi, 0, 1)')

# 9. Our full likelihood will now be:
# - PROD::name(pdf1,pdf2] : Create product of p.d.f with 'name' with given input p.d.fs
ws.factory('Poisson::poisN(N[15], nEvents)')
ws.factory('PROD::likelihood(poisN, lumiconstr)')

# 10. And the NLL is:
#    -  (Identical to step 5, except we feed in 'likelihood' instead of just 'poisN')
ws.factory('expr::NLL("-log(@0)", likelihood)')

# 11. Minimize:
nll = ws.function("NLL")   # retrieve the function with the given name                
minim = RooMinimizer(nll)  # RooMinimizer minimizes any RooAbsReal func. w.r.t its parameters    
minim.setErrorLevel(0.5)   # Wilks' theorem: -Delta NLL < 0.5                                    
minim.minimize("Minuit2", "migrad")    # this minimizes the function in the constructor             
bestfitnll = nll.getVal()  # getVal() directly from the function  





