# runDataCardCountingSimpleModel.sh
# https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/tutorial2020/exercise/#a-a-one-bin-counting-experiment

cmsenv

# Run FitDiagnostics on the datacard from the simple counting experiment we did at the end of Part 1
combine -M FitDiagnostics datacard_counting_simplemodel.txt --forceRecreateNLL

# - Optional: Open fitDiagnostics.root and fit_s->Print()

# Compare the pre- and post-fit values of the parameters, to see how much the fit to data has shifted
# and constrained these parameters with respect to the input uncertainty

python ../diffNuisances.py fitDiagnostics.root --all
